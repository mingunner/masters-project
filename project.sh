#!/bin/bash

python project.py
> ./grafi/nodi_significativi.txt
for grafo in AP BP random_A random_B; do
echo Significant nodes for graph $grafo >> ./grafi/nodi_significativi.txt;
echo >> ./grafi/nodi_significativi.txt;
tail --lines=176 ./grafi/rapporto_$grafo\.txt | cut -d: -f 1,8 | grep 'Cluster\|Close\|Far\|Eigen\|Katz\|InBetween\|PageRank' >> ./grafi/nodi_significativi.txt;
echo >> ./grafi/nodi_significativi.txt;
done
