from oct2py import octave
from math import log
import networkx as nx
import matplotlib.pyplot as plt
import random
import numpy
import sys

octave.eval('name_file') # It opens a .m file made with Octave containing two 176x176 adjacency matrices called AP and BP

AP=octave.pull('AP')
BP=octave.pull('BP')

def grafo(M): # Custom function for extracting a graph from an adjacency matrix M
	G=nx.Graph()
	for i in range(M.shape[0]):
		G.add_node(i+1)
		for j in range(M.shape[1]):
			if M[i,j]!=0:
				G.add_edge(i+1,j+1)
	G=nx.Graph(G)
	G.remove_edges_from(nx.selfloop_edges(G))
	return G


def trova(ogg,lista): # It seeks the first index in a list where an object ogg is found. There are way better implementations for this out there.
	c=-1
	i=0
	n=len(lista)
	trovato=False
	while i<n and not trovato:
		if lista[i]==ogg:
			c=i
			trovato=True
		i+=1
	return c


def kprob(G): # It returns [L0,L1,L2] where L0=degrees of nodes L1=degree probability distribution L2=cumulative probability 
	l=[[],[],[]]
	n=nx.number_of_nodes(G)
	for coppia in list(G.degree()): #L0
		if coppia[1] not in l[0]:
			l[0].append(coppia[1])
	l[0].sort()
	l[1]=[0]*len(l[0])
	l[2]=[0]*len(l[0])
	for coppia in list(G.degree()): #L1 as list of n(k)
		l[1][trova(coppia[1],l[0])]+=1
	for i in range(len(l[0])):  #L1 as list of n(k)/n
		l[1][i]=l[1][i]/float(n)
	for i in range(len(l[0])): #L2
		l[2][i]=sum(l[1][i:])
	return l


def grafo_casuale(G): # Random graph with the same "edge probability" of a graph G
	J=nx.create_empty_copy(G)
	nodi=list(J.nodes())
	n=len(nodi)
	max_archi=(n*(n-1))/2 # Number of edges of a complete undirected graph with n nodes
	archi=len(G.edges()) # number of edges. I could have used number_of_edges instead.
	prob=float(archi)/max_archi
	for i in range(n):
		for j in range(i+1,n):
			if random.random()<prob:
				J.add_edge(nodi[i],nodi[j])
	return J


def top_dict(dic): # Top10+ list of keys of a dictionary based on their values
	n=len(dic)
	l=sorted(dic.items(), key=lambda kv: kv[1],reverse=True) # Decreasing order of dic.items
	top=[] 
	for i in range(10): # Top 10 here
		top.append(l[i][0])
	i=10
	while i<n and l[i][1]==l[i+1][1]: # Extending the list with other keys if their value coincides with the tenth key's.
		top.append(l[i][0])
		i+=1
	return top


def elabora(G,nome): # Graph study
	n=nx.number_of_nodes(G)
	A=nx.adjacency_matrix(G).todense()
	#Generic data
	print 'Graph parameters '+nome,'\n'
	print 'Average clustering coefficient'
	print nx.average_clustering(G),'\n'
	print 'Network transitivity'
	print nx.transitivity(G),'\n'
	if nx.is_connected(G):
		distanza_media=nx.average_shortest_path_length(G)
		print 'Average distance'
		print distanza_media,'\n'
		print 'log n'
		print log(n),'\n'
	# Probabilistic study
	prob=kprob(G)
	print 'Erdos-Renyi probability p'
	p=float(nx.number_of_edges(G))*2/(n*(n-1))
	print p,'\n'
	print 'Average degree' #pag 235
	print round((n-1)*p),'\n'
	print 'Node degree probability distribution','\n'
	print 'Degree : Degree prob. : Degree cumul. prob','\n'
	for i in range(len(prob[0])): # using format so there are exactly three digits after the period
		print prob[0][i],':','{:.3f}'.format(prob[1][i]),':','{:.3f}'.format(prob[2][i])
	#Plot of probability distribution'
	#plt.ylabel('Probability')
	#plt.xlabel('Degrees')
	#plt.title('Grafo '+nome)
	#plt.plot(prob[0],prob[1],'bo')
	#plt.savefig('./grafi/dist_gradi_'+nome+'.svg',format='svg')
	#plt.clf()
	plt.ylabel('Cumulative probability',fontsize=25)
	plt.xlabel('Degrees',fontsize=25)
	plt.loglog(prob[0],prob[2],'bo',)
	plt.tick_params(labelsize=20,which='both')
	plt.tight_layout()
	plt.savefig('./grafi/dist_cumul_log_gradi_'+nome+'.png',format='png')
	plt.clf()
	print
	# Nodes analysis 
	print 'Node data','\n'
	cl=nx.clustering(G)
	cc=nx.closeness_centrality(G)
	ec=nx.eigenvector_centrality(G)
	top_ec=top_dict(ec)
	convergenza=False # Katz's centrality is a bit tricky
	alpha=min([0.1,max(abs(numpy.linalg.eig(A)[0]))**(-1)])
	while alpha>0.0001 and not convergenza:
		try:
			kc=nx.katz_centrality(G,alpha=alpha,max_iter=100)
		except nx.PowerIterationFailedConvergence:
			alpha=alpha/2
		else:
			convergenza=True
	if convergenza:
		top_kc=top_dict(kc)
		print 'Katz converges for alpha =',alpha,'\n'
	else:
		print 'No convergence for Katz','\n'
		top_kc=[]
		kc=[-1]*(n+1) # n+1 so there aren't index errors
	bc=nx.betweenness_centrality(G)
	pr=nx.pagerank(G)
	top_pr=top_dict(pr)
	print 'Node : Clustering : Closeness centr. : Eigenvector centr. : Katz centr. : Betweenness centr. : Page Rank : Notes','\n'
	for j in range(1,(n+1)):
		info=[]
		if cl[j]>0.85:
			info.append('Cluster')
		if cc[j]>0.6:
			info.append('Close')
		elif cc[j]<0.4:
			info.append('Far')
		if j in top_ec:
			info.append('Eigen')
		if j in top_kc:
			info.append('Katz')
		if bc[j]>0.5:
			info.append('InBetween')
		if j in top_pr:
			info.append('PageRank')
		print j,':','{:.3f}'.format(cl[j]),':','{:.3f}'.format(cc[j]),':','{:.3f}'.format(ec[j]),':','{:.3f}'.format(kc[j]),':','{:.3f}'.format(bc[j]),':','{:.3f}'.format(pr[j]),':',', '.join(info) # formatted string which will be fetched by tail command in project.sh
	nx.write_graphml(G,"./grafi/"+nome+".graphml") # graphml file for Cytoscape
	return


grafo_AP=grafo(AP)
grafo_BP=grafo(BP)


def rapporto():
	sys.stdout=open("./grafi/rapporto_AP.txt","w")
	elabora(grafo_AP,'AP')
	sys.stdout.close()
	sys.stdout=open("./grafi/rapporto_BP.txt","w")
	elabora(grafo_BP,'BP')
	sys.stdout.close()
	random_A=grafo_casuale(grafo_AP)
	sys.stdout=open("./grafi/rapporto_random_A.txt","w")
	elabora(random_A,'random_A')
	sys.stdout.close()
	random_B=grafo_casuale(grafo_BP)
	sys.stdout=open("./grafi/rapporto_random_B.txt","w")
	elabora(random_B,'random_B')
	sys.stdout.close()
	return

rapporto()

#
#
# End
#
#
