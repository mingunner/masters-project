# Master's degree project: analysis of a complex network

Project made by me for an exam during my master's.
An Octave file was handed to me by my professor, this file consisted in a collection of data gathered from a local school.

My python script reads that file with Oct2Py, than proceeds to create two graphs from the data and two random graphs based on the first ones.

Using NetworkX, these graphs are analysed (elabora() function) and for each one a .txt file, a .graphml file for Cytoscape and plots are generated. The txt file lists general info about the graph (such as the average shortest path) and a table where every entry is a node with its stats (clustering coefficient, centralities, etc.).

The bash scripts runs the python script and generates a fifth txt file with a collection of nodes that sport a special tag, which means they are somehow different from the majority of nodes.